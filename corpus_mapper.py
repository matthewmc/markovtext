#! /usr/bin/python
import sys

def map_():
    order = int(sys.argv[1])
    state = []

    for line in sys.stdin:
        # Clean up text
        line = line.lower()

        tokens = line.split()
        for token in tokens:
            # Print state-following word/key-value pair separated by a tab
            print('{0}\t{1}'.format(' '.join(state), token))
            state.append(token)
            # Limit state size by given parameter 'order'
            if len(state) > order:
                del state[0]

if __name__ == '__main__':
    map_()