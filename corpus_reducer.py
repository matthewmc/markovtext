#! /usr/bin/python
import sys

def reduce_():
    res = {}
    for pair in sys.stdin:
        pair = pair.split('\t')
        key = pair[0]
        value = pair[1]
        try: 
            res[key][value] += 1

        except KeyError:
            try: 
                res[key][value] = 1

            except KeyError: 
                res[key] = {}
                res[key][value] = 1
                
    # Loop through once-nested dict to return state, successive word, count
    for key in res.keys():
        for value in res[key].keys():
            count = res[key][value]
            print('{0}\t{1}\t{2}'.format(key, value, count))


if __name__ == '__main__':
    reduce_()

    